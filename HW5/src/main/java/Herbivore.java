import java.util.logging.Level;

public abstract class Herbivore extends Animal{
    public Herbivore(String name){
        super(name);
    }
    @Override
    public void eat(Food food) throws WrongFoodExeption {
        if(food instanceof Grass) {
            this.hungry = false;
        }
        else
          logger.error("Ошибка в кормлении!",new WrongFoodExeption());
    }
}