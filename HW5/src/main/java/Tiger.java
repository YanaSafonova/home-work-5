public class Tiger extends Carnivorous implements Thirsty{
    private boolean thirsty = true;
    public Tiger(String name){
        super(name);
        this.sizeAviary = SizeAviary.LARGE;
    }
    @Override
    public void drink() {
        this.thirsty = false;
    }

    @Override
    public void toilet() {
        this.thirsty = true;
    }

    @Override
    public void report() {
        if(thirsty)
            logger.info("Хочу пить");
        else
            logger.info("Хочу в туалет");
    }


    @Override
    public void voice() {
        logger.info("Ррррррррарррр");

    }
}