public class Eagle extends Carnivorous implements Sleep {
    private boolean sleeping = false;
    public Eagle(String name) {
        super(name);
        this.sizeAviary = SizeAviary.SMALL;
    }
        @Override
    public void sleep() {
        this.sleeping = true;
    }
    @Override
    public void wakeUp() {
        this.sleeping = false;
    }
    @Override
    public void eat(Food food) throws WrongFoodExeption {
        if(!this.sleeping)
            super.eat(food);
       else    logger.warn("Я не могу есть, я сплю! ");

    }

    @Override
    public void report() {
        if(this.sleeping)
            logger.info("Наелся и спит");

        else
            logger.info("Бодр и готов на свершения!");

    }

    @Override
    public void voice() {
        logger.info("Курлычет по-орлиному");
    }
}
