//import com.sun.org.slf4j.internal.Logger;
//import com.sun.org.slf4j.internal.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class Aviary <T extends Animal>{
    private static Logger logger = LoggerFactory.getLogger(Aviary.class);
    private int size;
    private SizeAviary sizeAviary;
    private int count = 0;
    private HashMap<String,T> animals = new HashMap<>();
    public Aviary(int size, SizeAviary sizeAviary){
        this.size = size;
        this.sizeAviary = sizeAviary;
    }
    public int getSize(){
        return this.size;
    }
    public void addAnimal(T animal){
        if(this.count >= this.getSize())
            logger.error("Вольер переполнен!");
        else if(animal.sizeAviary.ordinal() <= this.sizeAviary.ordinal()){
            this.count++;
            animals.putIfAbsent(animal.name,animal);
        }
        else
            logger.error("Вольер мал для данного животного!");
    }
    public void deleteAnimal(String name){
        if(animals.containsKey(name))
        animals.remove(name);
        else logger.error("Животного с такой кличкой нет!");

    }
    public T getAnimal(String name){
        if(!animals.containsKey(name))
            logger.error("Животного с такой кличкой нет!");
            return animals.get(name);

    }
}