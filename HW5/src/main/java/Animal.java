import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Animal {

protected static Logger logger = LoggerFactory.getLogger(Animal.class);
    protected boolean hungry = true;
    public String name;
    public SizeAviary sizeAviary;
    public Animal(String name){
        this.name = name;
    }
    public void getHungry(){
        if(this.hungry)
            logger.info("Голоден!");
        else
        logger.info("Покушал:)");
    }
    public abstract void eat(Food food) throws WrongFoodExeption;
    public abstract void voice();
}