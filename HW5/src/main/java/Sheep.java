public class Sheep extends Herbivore implements Thirsty{
    private boolean thirsty = true;
    public Sheep(String name){
        super(name);
        this.sizeAviary = SizeAviary.MEDIUM;
    }
    @Override
    public void drink() {
        this.thirsty = false;
    }

    @Override
    public void toilet() {
        this.thirsty = true;
    }

    @Override
    public void report() {
        if(thirsty)
            logger.info("Хочу пить");
        else
            logger.info("Хочу в туалет");
    }

    @Override
    public void voice() {
        logger.info("Беее");

    }
}